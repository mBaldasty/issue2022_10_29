package com.myclm;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentListManager {

    static private int currentId = 0;
    static private ConcurrentHashMap<Integer, Object> m_map = new ConcurrentHashMap<Integer, Object>();

    public static int setObject(Object obj) {
        int id = getId();
        m_map.put(id, obj);
        return id;
    }

    public static Object getObject(int id) {
        return m_map.getOrDefault(id, null);
    }

    public static Object removeObject(int id) {
        Object ret = m_map.getOrDefault(id, null);
        m_map.remove(id);
        return ret;
    }

    private static synchronized int getId() {
        return currentId++;
    }
}
