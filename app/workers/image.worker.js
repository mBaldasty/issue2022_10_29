require('@nativescript/core/globals');
import {isAndroid, isIOS} from "@nativescript/core";

let imageCorrection = isAndroid ? null : ImageCorrection

global.onmessage = function (msg) {
  let data = ConcurrentListManager.shared.getObject(msg.data.id)
  ConcurrentListManager.shared.removeObject(msg.data.id)
  if(isIOS) {
    let uiImage = imageCorrection.shared.adjustImageWithAsset(data)
    ConcurrentListManager.shared.setObject(uiImage)
    postMessage(1)
  } else {
  }
}
