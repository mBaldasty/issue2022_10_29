import Foundation
@objcMembers
@objc(ConcurrentListManager)
class ConcurrentListManager : NSObject {
    static let shared = ConcurrentListManager()
    static var currentId = 0;
    public static var accessQueue = DispatchQueue(label: "com.offizium.concurrentDictQueue", attributes: .concurrent)
    public static var underlyingDict = NSMutableDictionary.init()
    public static let waitingGroup = DispatchGroup() // possibly need to define in each function

    private override init(){

    }

    public static func getId() -> Int {
        currentId += 1
        return currentId
    }

    func getObject(_ id: Int) -> Any {
        var object: Any?
        ConcurrentListManager.accessQueue.sync(flags: .barrier) {
            object = ConcurrentListManager.underlyingDict.object(forKey: id)
        }
        return object
    }

    func setObject(_ object: Any) -> Int {
        ConcurrentListManager.waitingGroup.enter()
        ConcurrentListManager.accessQueue.async(flags: .barrier) {
            ConcurrentListManager.underlyingDict.setObject(object, forKey: ConcurrentListManager.getId() as NSCopying)
            ConcurrentListManager.waitingGroup.leave()
        }
        ConcurrentListManager.waitingGroup.wait()
        return ConcurrentListManager.currentId;
    }

    func removeObject(_ id: Int) {
        ConcurrentListManager.accessQueue.async(flags: .barrier) {
            ConcurrentListManager.underlyingDict.removeObject(forKey: id)
        }
    
        return
    }

    func removeAllObjects() {
        ConcurrentListManager.accessQueue.async(flags: .barrier) {
            ConcurrentListManager.underlyingDict.removeAllObjects()
        }
        ConcurrentListManager.underlyingDict = NSMutableDictionary.init()
        return
    }
}

/*
public class ConcurrentListManager : NSObject {
    public static let shared = ConcurrentListManager()
    public static var currentId = 0;
    public static var accessQueue = DispatchQueue(label: "com.offizium.concurrentDictQueue", attributes: .concurrent)
    public static var underlyingDict = NSMutableDictionary.init()
    public static let waitingGroup = DispatchGroup() // possibly need to define in each function

    private override init(){

    }

    public static func getId() -> Int {
        currentId += 1
        return currentId
    }

    public static func getObject(_ id: Int) -> Any {
        var object: Any?
        ConcurrentListManager.accessQueue.sync(flags: .barrier) {
            object = underlyingDict.object(forKey: id)
        }
        return object
    }

    public static func setObject(_ object: Any) -> Int {
        waitingGroup.enter()
        ConcurrentListManager.accessQueue.async(flags: .barrier) {
            underlyingDict.setObject(object, forKey: getId() as NSCopying)
            waitingGroup.leave()
        }
        waitingGroup.wait()
        return currentId;
    }

    public static func removeObject(_ id: Int) {
        ConcurrentListManager.accessQueue.async(flags: .barrier) {
            underlyingDict.removeObject(forKey: id)
        }
        return
    }

    public static func removeAllObjects() {
        ConcurrentListManager.accessQueue.async(flags: .barrier) {
            print(underlyingDict)
            print("clearing all")
            underlyingDict.removeAllObjects()
            print(underlyingDict)
        }
        return
    }
}
*/
