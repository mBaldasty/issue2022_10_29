import Foundation
import Photos
import UIKit
@objcMembers
@objc(ImageCorrection)
class ImageCorrection : NSObject {
    static let shared = ImageCorrection()

    private override init(){

    }

    func adjustImage(asset: PHAsset) -> String? {
            let uiimage = ImageCorrection.getUIImage(asset: asset)
            // return uiimage

            let imageData = uiimage.jpegData(compressionQuality: 1.0) ?? nil
            let imageBase64 = imageData?.base64EncodedString(options: [])
            return imageBase64 ?? nil

            // let returnUiimage = greyscale(uiimage)
            // let returnUiimage = monochromatic(uiimage)
            // return returnUiimage ?? uiimage
    }

    private static func getUIImage(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var image = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            image = result!
        })
        return image
    }

    private static func greyscale(_ image: UIImage) -> UIImage? {
        guard let currentCGImage = image.cgImage else {return nil}
        let currentCIImage = CIImage(cgImage: currentCGImage)
        let filter = CIFilter(name: "CIColorMonochrome")
        filter?.setValue(currentCIImage, forKey: "inputImage")
        filter?.setValue(CIColor(red: 0.7, green: 0.7, blue: 0.7), forKey: "inputColor")
        filter?.setValue(1.0, forKey: "inputIntensity")
        guard let outputImage = filter?.outputImage else {return nil}
        let context = CIContext()
        if let cgImg = context.createCGImage(outputImage, from: outputImage.extent) {
            let processedImage = UIImage(cgImage: cgImg)
            return processedImage
        } else {
            return nil
        }
    }

    private static func getDominantColor(_ image: UIImage) -> UIColor? {
        guard let currentCIImage = CIImage(image: image) else {return nil}
        let extentVector = CIVector(x: currentCIImage.extent.origin.x, y: currentCIImage.extent.origin.y, z: currentCIImage.extent.size.width, w: currentCIImage.extent.size.height)

        let filter = CIFilter(name: "CIAreaAverage", parameters: [kCIInputImageKey: currentCIImage, kCIInputExtentKey: extentVector])
        guard let outputImage = filter?.outputImage else {return nil}
        var bitmap = [UInt8](repeating: 0, count: 4)
        let context = CIContext(options: [.workingColorSpace: kCFNull])
        context.render(outputImage, toBitmap: &bitmap, rowBytes: 4, bounds: CGRect(x: 0, y: 0, width: 1, height: 1), format: .RGBA8, colorSpace: nil)
        let uiColor =  UIColor(red: CGFloat(bitmap[0]) / 255, green: CGFloat(bitmap[1]) / 255, blue: CGFloat(bitmap[2]) / 255, alpha: CGFloat(bitmap[3]) / 255)
        print(uiColor)
        return uiColor
    }

    private static func monochromatic(_ image: UIImage) -> UIImage? {
        let dominantColor = CIColor(color: getDominantColor(image) ?? UIColor(red: 255, green: 255, blue: 255, alpha: 1))
        guard let currentCGImage = image.cgImage else {print("first nil"); return nil}
        let currentCIImage = CIImage(cgImage: currentCGImage)
        let filter = CIFilter(name: "CIColorMonochrome")
        filter?.setValue(currentCIImage, forKey: "inputImage")
        filter?.setValue(dominantColor, forKey: "inputColor")
        filter?.setValue(1.0, forKey: "inputIntensity")
        guard let outputImage = filter?.outputImage else {print("second nil"); return nil}
        let context = CIContext()
        if let cgImg = context.createCGImage(outputImage, from: outputImage.extent) {
            let processedImage = UIImage(cgImage: cgImg)
            return processedImage
        } else {
            return nil
        }
    }
}
