import Vue from 'nativescript-vue'

import Home from './components/Home'
Vue.registerElement('CameraPlus', () => require('@nstudio/nativescript-camera-plus').CameraPlus);
import CollectionView from '@nativescript-community/ui-collectionview/vue';
Vue.use(CollectionView);
import * as imageModule from '@nativescript-community/ui-image'
import ImagePlugin from "@nativescript-community/ui-image/vue";
Vue.use(ImagePlugin)
imageModule.initialize({ isDownsampleEnabled: true })
Vue.registerElement('HTMLLabel', () => require('@nativescript-community/ui-label').Label);
require('@nativescript-community/ui-label').enableIOSDTCoreText();

Vue.config.silent = false

new Vue({
  render: (h) => h('frame', [h(Home)]),
}).$start()
